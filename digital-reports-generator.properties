# Node environment
NODE_ENV=production
#------------------------------------------------------------------------------
######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION START
#
#### E1. Entitlement interface :::: Optional ####
# Possible values: mock | active
# Default: active
# mock: Reads entitlement profile from mock_entl.js in <projectRoot/src> folder
# active: Connects to Entitlement Service
#
# AUTH_MODE=active

######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION START
#
#### Q1. Query store parameters used in development/test :::: Optional ####
# Possible values: true | false
# Default: true
# The query store is used as a convenience to load a query/variable set in Graphiql
#
# LOAD_QUERY_STORE=true

#### Q2. Default query set to be loaded :::: Optional ####
# If not provided no query will be loaded
#
# DEFAULT_QUERY_LOAD=accounts

######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## A. APPLICATION CONFIGURATION ######## ::: SECTION START
#### IMPORTANT: This must correspond to the schema definition ####
#
# A1. Application name used in Quest GraphQL Schema as Interface name :::: Required ####
APP_NAME_RPTGEN=ReportGeneratorService

#### A2. Application code for resolver package ::::  Required ####
# The projection store index name must correspond to this code (with lower casing applied)
APP_CODE_RPTGEN=RPTGEN

#### A3. Application Connector for resolver package ::::  Required ####
# The projection store connector option to be used
APP_CONNECTOR_RPTGEN=elasticSearch

#### A4. Application Connector Protocol for resolver package ::::  Required ####
# The projection store connector protocol option to be used
# NOTE: This will be removed eventually
APP_CONN_PROTOCOL_RPTGEN=http

######## A. APPLICATION CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION START
#
#### P1. CF Service Name for Elastisearch cluster :::: Optional ####
# Default value is elasticsearch
# Provide a value if you wish to override
# This is a Quest instance level configuration and will apply to all
# component packakges which will retrieve their configurations
#
# CF_SVC_NAME_ELASTICSEARCH=elasticsearch

#### P2. CF Service Name for specific package :::: Optional ####
# This is only applicable at an individual component package level
# The specific package will bind to the service name specified
# When provided in the Quest configuration serves as an override
#
# Overrides can be provided with a suffix
# ENTL - EntitlementsService
# ACCT - AccountServices
# ORG - OrgService
# FXRATE - FxRatesService
# BUSCAL - BusinessCalendarService
# REFDATA - ReferenceDataService
# LIQI - LiquidityICLService
# LIQS - LiquiditySweepsService
# LIQC - LiquidityCommonService
# PAY - PaymentsService
# CNR - CNRService
#
# Examples
# CF_SVC_NAME_ELASTICSEARCH_ENTL=entl-els-projection
# CF_SVC_NAME_ELASTICSEARCH_ORG=org-els-projection

#### P3. ES Connection string :::: Required ####
# This is only used in local development.
# Is not considered when deployed in CF
#
ES_CONNSTRING_RPTGEN=http://localhost:9200
#ES_CONNSTRING_ACCT=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_ORG=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_ENTL=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_BUSCAL=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_FXRATE=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_REFDATA=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com

#### P4. ES Server Partition for indexes :::: Optional ####
# Default value: N/A (empty)
# Indexes will be created/accessed with this prefix
# e.g. if Partition has a value of "DIT" then the index for Account Services will be "dit.acct"
#
ES_PARTITION=quest

#### P5. Response size (limit) for Search queries :::: Optional ####
# Default value: 2000
# Search queries will throttle the number of records retrieved based on this value
#
# ES_QUERY_LIMIT=2000

#### P6. Response size (limit) for Aggregate queries :::: Optional ####
# Default value: 50
# Aggregate queries will throttle the number of buckets retrieved based on this value
#
# ES_AGG_QUERY_LIMIT=50

#### P6. Timeout threshold in ms :::: Optional ####
# Default value: 30000 ms
#
# ES_TIMEOUT=30000

######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## E. REGISTRY CONFIGURATION ######## ::: SECTION START
#
#### E1. Enable discovery :::: Optional ###
# Possible values: true | false
# Default value: true
#
EUREKA_DISCOVERY_ENABLED=true

#### E2. Eureka discovery name :::: Required ####
# Default value: digital-quest
# If there are multiple quest contexts supported this must be provided
#
 EUREKA_DISCOVERY_NAME=digital-reports-generator

# EUREKA_QUEST_DISCOVERY_NAME=digital-quest

#### E3. Eureka service name :::: Optional ####
# Default value: digital-registry
# Eureka service name to connect for discovery services
#
# CF_SVC_NAME_DIGITAL_REGISTRY=digital-registry

#### E4. Eureka registration method :::: Optional ####
# Possible values: direct | route
# Default value: route
# Eureka registration supporting
# :: direct: Direct container access registering IP
# :: route: Route is registered
#
# EUREKA_REGISTRATION_METHOD=route

#### E7. Eureka Ports :::: Optional ####
# Port/Secure Port numbers for Route registration method
# Defaults:
# CF_APP_PORT_EUREKA=80
# CF_SECURE_APP_PORT_EUREKA=443
# EUREKA_ENABLE_SECURE_PORT=false

######## E. REGISTRY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## L. LOG CONFIGURATION ######## ::: SECTION START

#### L1. Logging level :::: Optional ####
# Supported values are
# error - Only errors are logged
# info - Information logs are enabled
# debug - Debug info is included in the logs
# Default value: info
#
LOG_LEVEL=info

#### L2. Console logging :::: Optional ####
# Directed to standard output
#
# CONSOLE_LOG=true

#### L3. Logs directed to file :::: Required ####
# Directed to file
# Default location is /logs/quest.log
#
# FILE_LOG=false

#### L4. File name for File Logs :::: Optional ####
# Will be available in logs folder
#
# FILE_LOG_NAME=quest.log

######## L. LOG CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION START
#
#### R1. Include error stack trace :::: Optional ####
# Possible values: true | false
# Default value: false
# Set to true if you require stack traces to be included in logs/ errors section in response
#
# ERROR_STACK_TRACE=false

#### R2. Include error path :::: Optional ####
#
# ERROR_PATH=false

#### R3. Include timer information in meta :::: Optional ####
# NOTE: To be made optional with default value as false
#
# TIMER=true

######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## S. SECURITY CONFIGURATION ######## ::: SECTION START

#### S1. User claims location :::: Optional ####
# Possible values: bearer | header
# Default value: bearer
# :: bearer: bearer id_token in Authorization header
# :: header: custom igtb_headers - igtb_user/igtb_domain
#
# USER_CLAIMS_LOCATION=bearer

#### S2. Validate User token :::: Optional ####
# Possible values: true | false
# Default value: true
# Verify signature and expiry of token
# NOTE: Must be able to toggle both of the above independently
#
USER_TOKEN_VALIDATION=false

######## S. SECURITY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION START

#### M1. Dependant packages :::: Optional ####
# NOTE: This will be phased out
#
# DEP_PKGS=true

#### M2. GraphIql enabled :::: Optional ####
# NOTE: This is for development/test usage
#
# GRAPHIQL_ENABLED=true

#### M3. Quest route prefix used in GraphIql :::: Optional ####
# Possible value: /quest
# Default value: N/A (empty)
# Required to support Graphiql route in Gatekeeper
#
# QUEST_ROUTE_PREFIX=

#### M4. Apollo engine configuration :::: Optional ####
# APOLLO_ENGINE=false
# APOLLO_ENGINE_KEY=service:prasvenk007-DigitalQuest:SGRBE4lyBnr4yU4R09W-HA
# APOLLO_TRACING=false
# APOLLO_CACHE_CONTROL=false

#### M5. NewRelic APM Configuration :::: Optional ####
# NEW_RELIC_LICENSE_KEY=9a0343bb1dd3aecf0b2da2e92302b503098cec38

#### M6. Development only :::: Optional ####
# If token validation is not required then custom headers can be provided
# for user identity transmission in Request
#
# DEV_USER=DHartm390
# DEV_DOMAIN=premierGroup

#### M7. Codacy project token :::: Optional ####
# Required for development
# Integration with Codacy
#
# CODACY_PROJECT_TOKEN=9892413b4c5d4ee38f5adf8b5f574446

#### M8. Greeting on Server Start :::: Optional ####
# GREETING=Welcome
# ES_SCROLL_CTX_LIVE=2m
#SUBSCRIPTIONS_ENABLED=false

LOG_MESSAGES=true
MT_BATCH_SIZE=2000
DEFAULT_BATCH_SIZE=10000
PDF_BATCH_SIZE=2000

# If component type is ingestion then additional ingestion related bootstrapping is required
QUEST_PACKAGE_TYPE=ingestion
# Kafka service URI
SVC_URI_KAFKA=elb-kafka-7e79bad516be639a.elb.us-east-1.amazonaws.com:9095
# SVC_URI_KAFKA=ec2-34-227-227-162.compute-1.amazonaws.com:9094
# Consumer Group Id - account-balances
KAFKA_CONSUMER_GROUPID=account-balances-group
# Kafka Topic for consumption 
KAFKA_SUBSCRIPTION_TOPIC=DIGI_REPORT_REQ_TOPIC
# Kafka Topic for production 
KAFKA_PRODUCER_TOPIC=DIGI_REPORT_RES_TOPIC

# Kafka DLQ Topic - For quarantining messages that failed processing
KAFKA_DLQ_TOPIC=DIGI_REPORT_DLQ_TOPIC
# Connection time out
KAFKA_CONN_TIMEOUT=30000
# Heartbeat Interval
KAFKA_HEARTBEAT_INTERVAL=3000
# Number of conn retries
KAFKA_CONN_RETRIES=5
# Ingestion Model
INGESTION_MODEL=ReportGenerator
# message Context
KAFKA_MESG_CONTEXT=digital-report-generator
KAFKA_ENABLED=true
KAFKA_SSL_ENABLED=false


#### S1. SFTP Host URI :::: Required ####
SFTP_HOST=ec2-18-209-136-27.compute-1.amazonaws.com
#### S2. SFTP Mount Path :::: Required ####
# Number of times to retry all requests to eureka
# Default value: 5
SFTP_PATH=/mnt/was-shared/LMS_HOME/ReportsByCommons/Reports/test
#### S3. SFTP UserName :::: Required ####
SFTP_USERNAME=Vwasadmin
#### S4. SFTP Request Maximum Retry :::: Required ####
# Number of times to retry all requests to eureka
# Default value: 5
SFTP_PORT=22
#### SFTP PRIVATE KEY TYPE :::: Required ####
#SFTP_PRIVATE_KEY_TYPE=string
#### S5. SFTP Private Key :::: Required ####
#SFTP_PRIVATE_KEY="-----BEGIN RSA PRIVATE KEY-----\nMIIEoQIBAAKCAQEAoUju+/SS6J5bH/QTbOnmJe5V+o7b9xnQ9ukUME23ZOWpRN2c\nR9l1zFLxNT4FjDvJnWNs+zWXv2anQAgCBfsX6ddJPuSO8hwAzU5ddb+qz0Frq12E\nDZtUGmu3+zpoTtSG9QCLNjoFeth66csVWH8XbABiN2RAWdEn4q0Ac1BTwjB2NZAw\nHTDWvHqzPOl19kwpR+cQwaU7OziMGQZABBQRM6MKPxqi7SoQ4QJMip4kmM+aUQe/\nV811PdhnDqXL5Gjp/J8VAv1+k8qKt3uT7mo6XDfdwMqxfQC7SOmYvtTVMfkksjPb\ng7FBj++W6KGy6iaUamyvntUyDrmc9pyByguotQIBJQKCAQEAlDUuolZde8/JW6IE\nAzfMkY7nOUUBe0gnziJJ5zKohjrS5VYFOxra8xvI5NE1h8g8uiQDR7S09RI40w5H\nDGoxpnLNr2o+KpZF7ReUJwMnVqrRpGPFdEmLjeZxtm0F55Lc4SMYI/33Mp1qB0T+\n27MOmpiRmq8mYF9VFXxhR16SKA9l4JENiahxEIZ8fPH6ukcHO8suzX4Aox7L3wNv\n26fCGpXRSCcJXiD5y5ME3dShw2Cr0xQAFInTKOiHO7O3RTB+68pr8DA04zy3rp83\n+5mF+4bfBH11L1/fEsti2dTNxb+GYSNVq93wJtPN6yOEa1NHMOKUWhOPBQHn8nLq\n+oQtZQKBgQDMe/NQpLKBwteyIe0fCdeQLt1kPnN+z2edXfJR0X897GoGHCAMLpoE\nQUd9aZtBfBKp9hkrS2OxY3j3jkfPOlXLVElBWdPIRey/5juUEcr8A0wBvRAVr2l2\n/Ud01BkOyzZvebKiGZppR7bQMgzrKsySkxTKFFQ8+2/twpUuTLkzswKBgQDJ6uDn\nU6FhdrZMdOXzP9wCKsquSTesqz56SGMDh080wOFGCSp19j78ybjveubJqgDqE1jQ\nMkw/8Z6gGcAPk3x2a4ES54vXGlfz3oOsWeGV9GCeBl9B7Nwc1UxWdicElxfuR0Qv\nAeGPLQb9NrHk0hX4IiUW7D5/FV4yyk4Ie8Kd9wKBgEfYhevm1u9SS8j3Kcyil9i2\nhSM4iXG+flnivOxQhqcN4A/8C0KM6gF+uEDSE/Rp3QtIoQ82KfI3tOFbgQOe39+n\n/hAKzd6VG9S4rSYvw9wBKIr9PQCzQLsauD3N+vdcJ+H6U4UP6iT9hWu+jurXr6ke\n66DyYslRbIP4QkCzLFBvAoGAfYQq8Kmbq0nLREiqniDHCEQdIDtZ9baOnwpnEAgA\nzcQBqBOJGOUuEsKVjfJzyXeEByDIgWtf1LjKY4Wg5xZ9zRJk/enhTlx73MiXCkW1\nvhRzaSaOPbzALZlY93ng5y19lB52fhXtE8/hv/9nqepgrwB366eceILnh1splpIf\nCD8CgYAZJkJsdBbWkuYNB66ezOh2Mgss/KnUdni2P61cFfABxP3RoejUHmwesfKr\nUA5qbdgA9hG4O2QI7CCUBYm0w+rXXMU+ZL9fLNA11Ag6jsZk55wvbQwDu4JjyfLY\n4znLu09WxmATGU/KG7+q367tfS24aaJHajwJxHuhLyMYXYb6FQ==\n-----END RSA PRIVATE KEY-----\n"
#SFTP_PRIVATE_KEY=config/ireleaseVwasadmin.ppk
#SFTP_PRIVATE_KEY=-----BEGIN RSA PRIVATE KEY-----\nMIIEoQIBAAKCAQEAoUju+/SS6J5bH/QTbOnmJe5V+o7b9xnQ9ukUME23ZOWpRN2c\nR9l1zFLxNT4FjDvJnWNs+zWXv2anQAgCBfsX6ddJPuSO8hwAzU5ddb+qz0Frq12E\nDZtUGmu3+zpoTtSG9QCLNjoFeth66csVWH8XbABiN2RAWdEn4q0Ac1BTwjB2NZAw\nHTDWvHqzPOl19kwpR+cQwaU7OziMGQZABBQRM6MKPxqi7SoQ4QJMip4kmM+aUQe/\nV811PdhnDqXL5Gjp/J8VAv1+k8qKt3uT7mo6XDfdwMqxfQC7SOmYvtTVMfkksjPb\ng7FBj++W6KGy6iaUamyvntUyDrmc9pyByguotQIBJQKCAQEAlDUuolZde8/JW6IE\nAzfMkY7nOUUBe0gnziJJ5zKohjrS5VYFOxra8xvI5NE1h8g8uiQDR7S09RI40w5H\nDGoxpnLNr2o+KpZF7ReUJwMnVqrRpGPFdEmLjeZxtm0F55Lc4SMYI/33Mp1qB0T+\n27MOmpiRmq8mYF9VFXxhR16SKA9l4JENiahxEIZ8fPH6ukcHO8suzX4Aox7L3wNv\n26fCGpXRSCcJXiD5y5ME3dShw2Cr0xQAFInTKOiHO7O3RTB+68pr8DA04zy3rp83\n+5mF+4bfBH11L1/fEsti2dTNxb+GYSNVq93wJtPN6yOEa1NHMOKUWhOPBQHn8nLq\n+oQtZQKBgQDMe/NQpLKBwteyIe0fCdeQLt1kPnN+z2edXfJR0X897GoGHCAMLpoE\nQUd9aZtBfBKp9hkrS2OxY3j3jkfPOlXLVElBWdPIRey/5juUEcr8A0wBvRAVr2l2\n/Ud01BkOyzZvebKiGZppR7bQMgzrKsySkxTKFFQ8+2/twpUuTLkzswKBgQDJ6uDn\nU6FhdrZMdOXzP9wCKsquSTesqz56SGMDh080wOFGCSp19j78ybjveubJqgDqE1jQ\nMkw/8Z6gGcAPk3x2a4ES54vXGlfz3oOsWeGV9GCeBl9B7Nwc1UxWdicElxfuR0Qv\nAeGPLQb9NrHk0hX4IiUW7D5/FV4yyk4Ie8Kd9wKBgEfYhevm1u9SS8j3Kcyil9i2\nhSM4iXG+flnivOxQhqcN4A/8C0KM6gF+uEDSE/Rp3QtIoQ82KfI3tOFbgQOe39+n\n/hAKzd6VG9S4rSYvw9wBKIr9PQCzQLsauD3N+vdcJ+H6U4UP6iT9hWu+jurXr6ke\n66DyYslRbIP4QkCzLFBvAoGAfYQq8Kmbq0nLREiqniDHCEQdIDtZ9baOnwpnEAgA\nzcQBqBOJGOUuEsKVjfJzyXeEByDIgWtf1LjKY4Wg5xZ9zRJk/enhTlx73MiXCkW1\nvhRzaSaOPbzALZlY93ng5y19lB52fhXtE8/hv/9nqepgrwB366eceILnh1splpIf\nCD8CgYAZJkJsdBbWkuYNB66ezOh2Mgss/KnUdni2P61cFfABxP3RoejUHmwesfKr\nUA5qbdgA9hG4O2QI7CCUBYm0w+rXXMU+ZL9fLNA11Ag6jsZk55wvbQwDu4JjyfLY\n4znLu09WxmATGU/KG7+q367tfS24aaJHajwJxHuhLyMYXYb6FQ==\n-----END RSA PRIVATE KEY-----
#### S6. SFTP Request Maximum Retry :::: Optional ####
# Number of times to retry all requests to eureka
# Default value: 5
SFTP_MAX_RETRIES=5
#### S7. SFTP Request Retry Delay :::: Optional ####
# milliseconds to wait between retries. This will be multiplied by the # of failed retries.
# Default value: 5000
SFTP_RETRY_DELAY=5000


ES_LOG_LEVEL=info
